# Bit�cora de trabajo: Whitecat

El equipo de Rob�tica llamado Whitecat entro a las tres categor�as del concurso de Inter preparatorias el 12 de abril del 2019, siendo dos integrantes que hab�an entrado con anterioridad a un concurso del mismo tipo en 2017.

## D�a 1, Lunes 10 de Abril
Emm el d�a en el que vinieron unas personas a dar talleres de la t�cnica sistemas computacionales, fui a la �ltima conferencia y vi que la persona que dio la conferencia, pues llego muy lejos en muy poco tiempo. Le pregunte como lo hizo y me dijo que gano varios torneos y concursos, as� que decid� unirme a rob�tica por eso.
### _Daniel_

## D�a 2, Jueves 11 de Abril
Definitivamente me unir� a rob�tica, as� que le dije a Ricardo, "vamos a unirnos a rob�tica" y el dijo que ok, as� que tambi�n se uni� a rob�tica:)
### _Daniel_

Me uni a robotica de nuevo porque cre� que seria un solo robot y una sola categoria, cuando me uni y vi que RoboRobo era un poco m�s facil que soldar cosas en Arduino, me interese un poco m�s. Pero llego el dia cuando me dijieron que ser�an tres
categorias con robots distintos, y al parecer no puedo salir ahora de Robotica.
### _Ricardo_

## D�a 3, Viernes 12 de Abril
Vino el encargado de rob�tica, y nos pidi� nuestros nombres para poder durante las vacaciones (porque somos gente bien trabajadora jajaja).
### _Daniel_

## D�a 4, Lunes 15 de Abril
Empezamos a hacer el robot, y quisimos aprender, Ricardo y yo, primero como funciona RoboRobo, y en ese primer d�a, hicimos la primera versi�n funcional del robot. Solo que no ten�a los 3 sensores que ocupabamos, solo era para controlar por remoto.
### _Daniel_

## D�a 5, Martes 16 de Abril
Ya no pod�amos ponerle muchas cosas al robot por lo que su potencial de desarrollo ya no llego a m�s, as� que lo desarmamos todo el segundo d�a, y planeamos ahora si, ponerle sensores de 

## No se hizo nada
_Ricardo y yo ibamos a veces a rob�tica pero como no hab�an ideas, o motivaci�n, no se hac�a nada significativo.

## D�a 6, Jueves 25 de Abril
Modifique los controles, para que se movieran bien (porque se mov�an al reves)
### _Ricardo_

## D�a 7, Martes 30 de Abril
Estamos extendiendo la bit�cora, y trabajaremos con la garra. Es que no empezamos la bit�cora cuando debiamos.
### _Daniel_

## D�a 8, Mi�rcoles 1 de Mayo
Vinimos a rob�tica a hacer unas pruebas, y probando varias formas de hacer la garra. Alguien nuevo entr� a rob�tica y se llama Miguel. Esperemos que coopere muy bien:)
### _Daniel_

## D�a 9, Jueves 2 de Mayo
Empezamos a desarrollar una idea de la garra, y no funcion�.
### _Daniel_

## D�a 10, Viernes 3 de Mayo
Hay un nuevo equipo para WRO en un concurso para despu�s, y me gustar�a ayudarlos, y como su encargado no los ayuda, decid� hacerlo yo, para aprender a ense�ar.
### _Daniel_

## D�a 11, S�bado 4 de Mayo
Me puse a ayudarlos, se llaman Kevin, Debani y Abi.
El robot que Ricardo y yo estuvimos haciendo lo desarmamos completamente otra vez, junto con Miguel porque el robot ya no ten�a potencial de desarrollo.
### _Daniel_

## Semana de descanso
_Ricardo y yo teniamos muchas cosas que hacer, as� que decidimos no ir a rob�tica por una semana para avanzar con nuestro otro proyecto._

## D�a 12, Lunes 13 de Mayo
Abi empez� a trabajar en rob�tica conmigo. Tanto Miguel como Ricardo dejaron rob�tica porque ya no les gust�, por lo que qued� solo en mi equipo.
Estuvimos haciendo la programaci�n para el robot con un solo sensor, pero no pude.
### _Daniel_

## D�a 13, Martes 14 de Mayo
Hicimos una nueva garra para el robot con cinta aislante roja. 
### _Daniel_

## D�a 14, Mi�rcoles 15 de Mayo
Trabaje con Ecobot, haciendo que el robot pudiera recoger objetos bien. No me gust� pero es lo que pude hacer por ahora.
### _Daniel_

## D�a 15, Jueves 16 de Mayo
No tantas cosas significantes. El concurso es en 2 d�as
### _Daniel_

## D�a 16, Viernes 17 de Mayo
Terminamos de la mejor manera Ecobot, y ahora mismo estamos trabajando con el robot del laberinto.
El robot de SuperTeams es muy probable que lo hagamos en el concurso
### _Daniel_ 
